function makeuser(nameValue, ageValue, addValue, telValue, sexValue, emailValue) 
{
	return {name : nameValue,
			age : ageValue,
			address : addValue,
			tel : telValue,
			sex : sexValue,
			email : emailValue,};
}

function showUser(argument) 
{
			let str = "";
			for (let key in argument){
				str = str+argument[key]+"\n";
			}
			return str;
}

function cloneUser(user)
{
	let tmpUser = {};
	for(let key in user)
	{
		tmpUser[key] = user[key];
	}
	return tmpUser;

}